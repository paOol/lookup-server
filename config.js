module.exports = {
  //
  server: {
    // Which port the server should listen for requests on.
    port: 8585,

    // Where to store the servers database file(s).
    database: './database.db',

    // What data should be cached by the server.
    // 1: Minimal (Lookup transaction data and inclusion proofs on-demand)
    // 2: Default (Store transaction data and inclusion proofs in database)
    storage: 2,

    // Should we enable pre-parsed metadata requests?
    metadata: true,

    // Should we enable free registrations?
    register: true
  },

  // Location and credentials to connect to a fullnode RPC.
  node: {
    // The address where we can reach a Bitcoin RPC service.
    address: process.env.HOST,
    // The port on which the Bitcoin RPC service listens to.
    port: process.env.PORT,

    // The username and password for the Bitcoin RPC service.
    user: process.env.USER,
    pass: process.env.PASSWORD
  }
};
