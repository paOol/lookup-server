#IMAGE BUILD COMMANDS
FROM ubuntu:18.04

#Update the OS and install any OS packages needed.
RUN apt-get update
RUN apt-get install -y sudo git curl nano gnupg

#Install Node and NPM
RUN curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs build-essential

#Create the user 'node' and add them to the sudo group.
RUN useradd -ms /bin/bash node
RUN adduser node sudo

#Set the working directory to be the connextcms home directory
WORKDIR /home/node

# Expose the port the API will be served on.
EXPOSE 8585

# Switch to user account.
USER node

# Clone the app repository
WORKDIR /home/node
RUN git clone https://gitlab.com/paOol/lookup-server.git

WORKDIR /home/node/lookup-server

# Install dependencies
RUN npm install


# COPY config/start-mainnet /home/node/lookup-server/start-mainnet
# CMD ["/home/node/lookup-server/start-mainnet"]`

CMD ["npm", "run", "dev"]


