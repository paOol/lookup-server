SELECT
	account_emoji,
	name_text,
	account_number,
	account_hash,
	account_collision_length,
	payload_type,
	payload_address

FROM payloads
LEFT JOIN account_payloads USING (payload_id)
LEFT JOIN accounts USING (account_id)
LEFT JOIN names USING (name_id)

WHERE payload_address = :address;
