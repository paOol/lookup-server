// Enable support for Express apps.
const express = require('express');
const router = express.Router();

let Utils = require('slpjs').Utils;

let convertToSLP = addr => {
  return Utils.toSlpAddress(addr);
};
let convertToCashAddr = addr => {
  return Utils.toCashAddress(addr);
};

let formatSLP = results => {
  let clean = results.map(x => {
    let obj = x;
    if (x.payload_type === 129) {
      obj.payload_address = convertToSLP(x.payload_address);
    }
    return obj;
  });
  return clean;
};

router.get('/:address', async function(req, res) {
  const { address } = req.params;

  // Notify the server admin that a lookup request has been received.
  req.app.locals.debug.server(
    'Registration transaction(s) requested by ' + req.ip
  );
  req.app.locals.debug.struct('Validating lookup request input fields.');

  // Initialize an empty response object.
  let lookupResult = {};

  const isSLP = Utils.isSlpAddress(address);
  const isBCH = Utils.isCashAddress(address);

  // Validate that the account number is in the given range.
  if (!isSLP && !isBCH) {
    lookupResult.error = 'Not a valid BCH address.';
  } else {
    // add bch prefix because database addresses contain them
    req.params.address = convertToCashAddr(address);
  }

  req.app.locals.debug.struct(
    'Completed validation of lookup request input fields.'
  );

  // If validation failed..
  if (typeof lookupResult.error != 'undefined') {
    // Notify the server admin that this request was invalid.
    req.app.locals.debug.server(
      'Delivering error message due to invalid request to ' + req.ip
    );
    req.app.locals.debug.object(lookupResult);

    // Return a 400 BAD REQUEST response.
    return res.status(400).json(lookupResult);
  }

  try {
    let databaseLookupResult = null;

    // Query the database for the result.
    databaseLookupResult = req.app.locals.queries.reverseLookup.all(req.params);

    // If no result could be found..
    if (
      typeof databaseLookupResult == 'object' &&
      Object.keys(databaseLookupResult).length == 0
    ) {
      // Notify the server admin that this request has no results.
      req.app.locals.debug.server(
        'Delivering error message due to missing results ' + req.ip
      );

      // Return 404 eror.
      return res
        .status(404)
        .json({ error: 'No account matched the requested parameters.' });
    }

    // allow simpleledger format in results if searching slp
    databaseLookupResult = formatSLP(databaseLookupResult);

    // Add the final data to the result of the response object.
    lookupResult.results = databaseLookupResult;

    // Notify the server admin that the request has been processed.
    req.app.locals.debug.server(`addresses(s) delivered to ${req.ip}`);
    req.app.locals.debug.object(lookupResult);

    // Return a 200 OK with the lookup result.
    return res.status(200).json(lookupResult);
  } catch (error) {
    // Log an error for an administrator to investigate.
    req.app.locals.debug.errors('Failed to lookup account:', error);

    // Return a 500 Internal Server Error.
    return res.status(500);
  }
});

module.exports = router;
