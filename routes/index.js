// Enable support for Express apps.
const express = require('express');
const router = express.Router();

router.get('/', async function(req, res) {
  let html = `
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Cash Accounts is a decentralized alias system working on Bitcoin Cash.">
        <meta name="author" content="Cash Accounts">

        <meta property="og:image" content="https://badger.bitcoin.com" />
        <meta name="twitter:image" content="https://badger.bitcoin.com">
        <meta name="twitter:image:src" content="https://badger.bitcoin.com">

        <meta property="og:locale" content="en_US" />
        <meta property="og:title" content="">
        <meta property="og:description" content="Cash Accounts is a decentralized alias system working on Bitcoin Cash.">
        <title>cashaccounts.bitcoin.com</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/style.css">

    </head>

    <body>

        <div class="profile">
            <section id="wrapper">
                <header id="header">

                    <a>
                        <img id="logo" class="2x" src="/logo.jpg" />
                    </a>

                    <h1>Cash Accounts</h1>
                    <h2>A decentralized alias system working on <span class="highlight"> Bitcoin Cash.</span></h2>
                </header>
            </section>
        </div>

        <section id="wrapper" class="home">
            <div class="about">

                <ol class="list-view">

                    <li class="list-item">
                        <div class="list-header">
                            <div class="_bubble" style="background-image: url(/colored_credit_card.svg);"></div>
                        </div>
                        <div class="list-detail">
                            <div class="_title-block">
                                <h3>Readable Alias</h3>
                            </div>
                            <p class="_summary"> Human readable accounts so you can send to <a href="/display/jonathan/100" target="_blank"> Jonathan#100</a> instead of having to memorize <i>bitcoincash:qp3wjpa3tjlj042z2wv7hahsldgwhwy0rq9sywjpyy</i> </p>
                        </div>
                    </li>

                    <li class="list-item">
                        <div class="list-header">
                            <div class="_bubble" style="background-image: url(/colored_security-69.svg);"></div>
                        </div>
                        <div class="list-detail">
                            <div class="_title-block">
                                <h3>Decentralized</h3>
                            </div>
                            <p class="_summary"> Registrations and lookups are public on the Bitcoin Cash blockchain. There is no risk of a central server being hacked and replacing everyones alias with the attacker's address.</p>
                        </div>
                    </li>

                    <li class="list-item">
                        <div class="list-header">
                            <div class="_bubble" style="background-image: url(/colored_loan-63.svg);"></div>
                        </div>
                        <div class="list-detail">
                            <div class="_title-block">
                                <h3>Token Support</h3>
                            </div>
                            <p class="_summary"> Expandable format includes support for popular implementations like Simple Ledger Protocol tokens. For example: <a href="/display/tokenAware/15874" target="_blank"> tokenAware#15874</a>.</p>
                        </div>
                    </li>

                    <li class="list-item">
                        <div class="list-header">
                            <div class="_bubble" style="background-image: url(/colored_crypto-92.svg);"></div>
                        </div>
                        <div class="list-detail">
                            <div class="_title-block">
                                <h3>API</h3>
                            </div>
                            <p class="_summary">
                                Lookups can be done with a simple endpoint such as <a href="/account/100/jonathan" target="_blank">/account/100/jonathan</a>. If you're a developer, <a href="https://rest.bitcoin.com/#/cashAccounts" target="_blank"> Learn More Here.</a> </p>
                        </div>
                    </li>
                    <li class="list-item">
                        <div class="list-header">
                            <div class="_bubble" style="background-image: url(/colored_blockchain-93.svg);"></div>
                        </div>
                        <div class="list-detail">
                            <div class="_title-block">
                                <h3>More Info</h3>
                            </div>
                            <p class="_summary">For more information visit <a target="_blank" href="https://www.cashaccount.info/">https://www.cashaccount.info</a>. For Developers, looking to integrate cash accounts, check out the
                                <a target="_blank" href="https://github.com/Bitcoin-com/cashaccounts">Javascript library</a>.

                            </p>
                        </div>
                    </li>
                </ol>
            </div>

        </section>

    </body>

    </html>
  `;
  // Return a 200 OK.
  return res.status(200).send(html);
});

module.exports = router;
